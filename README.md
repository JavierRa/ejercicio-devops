# Ejercicio DevOps

¡Bienvenido a este repositorio de código creado con fines académicos!

Este repositorio es un espacio dedicado al intercambio y colaboración en el ámbito académico, donde un grupo de alumnos realizará un ejercicio para demostrar sus conocimientos en el uso de Git.

## Lista de Nombres

* Arturo - Squad Acadmémico
* Jose - Squad 10
* Pedro - Squad 1
* Javier - Squad 2
---
![Licencia de Creative Commons 4.0](https://i.creativecommons.org/l/by/4.0/88x31.png) El uso de esta obra está compartida bajo una [licencia de Creative Commons Reconocimiento 4.0 Internacional](http://creativecommons.org/licenses/by/4.0/).